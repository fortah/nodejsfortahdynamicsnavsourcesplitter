# Dynamics NAV Source Code Splitter

Jakub Hojnacki <jakubhojnacki@gmail.com>

## Overview

The application splits multi-object file or files with C/AL code into a set of files, each containing one object.

## Usage

Usage:

```shell
node "./src/main.mjs" "{SourceDirectoryPath}" "{DestinationDirectoryPath}" (-m "{SourceFileMask}") (-ddp "{DestinationDirectoryNamePattern}") 
  (-dfp "{DestinationFileNamePattern}") (-de)
```

Where:

| Parameter                                | Description                                                  |
| ---------------------------------------- | ------------------------------------------------------------ |
| "{SourceDirectoryPath}"                  | Path to directory with source (multi-object) files           |
| "{DestinationDirectoryPath}"             | Path to directory for destination (single-object) files. Has to be different than {SourceDirectoryPath} |
| -m "{SourceFileMask}"                    | Mask for source files. It can contain a number of semicolon-separated file patterns with wildcards (like \*.txt).<br />If the parameter is not passed, the application uses a default pattern for Dynamics NAV, which is: \*.txt;\*.cal |
| -ddp "{DestinationDirectoryNamePattern}" | Pattern for destination (sub) directory. If empty - the system will place all destination files together. <br />For any non-empty - the system will work out and use (sub) directory name for each file. <br />It can contain a number of placeholders:<br />- {type} - Object type<br />- {typePlural} - Object type in plural form<br />- {typeLowerCase} - Object type lowercase<br />- {typePluralLowerCase} - Object type plural and lowercase |
| -dfp "{DestinationDirectoryNamePattern}" | Pattern for destination file name. <br />When it is not specified, the application will use a default one, which is: "{type} {name} ({id})" (for instance: "table Sales Header (36)") <br />It can contain a number of placeholders:<br />- {type} - Object type<br />- {typePlural} - Object type in plural form<br />- {typeLowerCase} - Object type lowercase<br />- {typePluralLowerCase} - Object type plural and lowercase<br />- {no} - Object number (ID)<br />- {name} - Object name |
| -de                                      | Diagnostic mode is enabled. The application will output more information regarding its flow |

## Version History

### 2.1.0 - 2022-03-08 - JH
- New libraries structure and names