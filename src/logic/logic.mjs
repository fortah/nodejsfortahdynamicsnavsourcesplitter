/**
 * @module "Logic" class
 * @description Represents application logic
 */

import FileSystem from "fs";
import Path from "path";
import ReadLine from "readline";

import { DynamicsObject } from "fortah-dynamics-library";
import { FileSystemToolkit } from "fortah-file-system-library";
import { LanguageToolkit } from "fortah-core-library";
import { Validator } from "fortah-core-library";

export class Logic {

    static get defaultFileMask() { return "*.txt;*.cal"; }
    static get defaultDirectoryNamePattern() { return ""; }
    static get defaultFileNamePattern() { return "{type} {name} ({no}).cal"; }
    static get sourceFileRegexPattern() { return "object\\s+(?<type>table|form|report|dataport|codeunit|xmlport|page|query|menusuite)\\s+(?<no>\\d+)\\s+(?<name>.*)"; }

    get application() { return global.theApplication; }
    
    get sourceFileRegex() { return this.mSourceFileRegex; }
    set sourceFileRegex(pValue) { this.mSourceFileRegex = pValue; }
    get destinationFile() { return this.mDestinationFile; }
    set destinationFile(pValue) { this.mDestinationFile = pValue; }

    constructor() {
        this.sourceFileRegex = new RegExp(Logic.sourceFileRegexPattern, "i");
        this.destinationFile = null;
    }

    async run() {
        let result = false;
        if (this.validate()) {
            await this.process();
            result = true;
        }
        return result;
    }

    validate() {
        const validator = new Validator();
        if (this.application.args.sourceDirectoryPath == this.application.args.destinationDirectoryPath)
            validator.addError("Source folder path can''t be the same as destination folder path.");
        return validator.requireSuccess();
    }		

    async process() {
        const sourceFiles = FileSystemToolkit.readDirectoryFiles(this.application.args.sourceDirectoryPath, this.application.args.sourceFileMask);
        for (const sourceFile of sourceFiles) {
            this.application.messenger.sendInformation(`${sourceFile.name} -->`);
            const stream = FileSystem.createReadStream(Path.join(this.application.args.sourceDirectoryPath, sourceFile.name));
            const reader = ReadLine.createInterface({ input: stream, crlfDelay: Infinity });		
            for await (const line of reader)
                this.processLine(line);
            this.closeDestinationFile();
        }
    }

    processLine(pLine) {
        const result = this.sourceFileRegex.exec(pLine);
        if (result) {
            this.closeDestinationFile();
            this.openDestinationFile(new DynamicsObject(result.groups.type, result.groups.no, result.groups.name));
        }
        FileSystem.writeSync(this.destinationFile, pLine + "\r\n");
    }

    openDestinationFile(pObject) {
        const destinationDirectoryName = this.createDestinationName(this.application.args.destinationDirectoryNamePattern, pObject);
        const destinationDirectoryPath = Path.join(this.application.args.destinationDirectoryPath, destinationDirectoryName);
        if (!FileSystem.existsSync(destinationDirectoryPath))
            FileSystem.mkdirSync(destinationDirectoryPath);

        const destinationFileName = this.createDestinationName(this.application.args.destinationFileNamePattern, pObject);
        const destinationFilePath = Path.join(destinationDirectoryPath, destinationFileName);
        this.destinationFile = FileSystem.openSync(destinationFilePath, "w");

        this.application.messenger.sendInformation(`--> ${destinationFileName}`);
    }

    createDestinationName(pPattern, pObject) {
        let name = pPattern;
        name = name.replace("{type}", pObject.type);
        if (name.indexOf("{typePlural}") >= 0)
            name = name.replace("{typePlural}", LanguageToolkit.toPlural(pObject.type));
        if (name.indexOf("{typeLowerCase}") >= 0)
            name = name.replace("{typeLowerCase}", pObject.type.toLowerCase());
        if (name.indexOf("{typePluralLowerCase}") >= 0)
            name = name.replace("{typePluralLowerCase}", LanguageToolkit.toPlural(pObject.type.toLowerCase()));
        name = name.replace("{no}", pObject.no);
        name = name.replace("{name}", pObject.name);
        name = FileSystemToolkit.toName(name);
        return name;
    }

    closeDestinationFile() {
        if (this.destinationFile) {
            FileSystem.closeSync(this.destinationFile);
            this.destinationFile = null;
            this.noOfFilesGenerated++;
        }
    }

}
