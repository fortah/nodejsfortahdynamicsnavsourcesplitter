"use strict";

import { ArgsBase } from "fortah-core-library";
import { ArgName } from "./argName.mjs";
import { ArgTemplateFactory } from "./argTemplateFactory.mjs";

export class Args extends ArgsBase {

    get application() { return global.theApplication; }

    get sourceDirectoryPath() { return this.mSourceDirectoryPath; }
    set sourceDirectoryPath(pValue) { this.mSourceDirectoryPath = String.verify(pValue); }
    get destinationDirectoryPath() { return this.mDestinationDirectoryPath; }
    set destinationDirectoryPath(pValue) { this.mDestinationDirectoryPath = String.verify(pValue); }
    get sourceFileMask() { return this.mSourceFileMask; }
    set sourceFileMask(pValue) { this.mSourceFileMask = String.verify(pValue); }
    get destinationDirectoryNamePattern() { return this.mDestinationDirectoryNamePattern; }
    set destinationDirectoryNamePattern(pValue) { this.mDestinationDirectoryNamePattern = String.verify(pValue); }
    get destinationFileNamePattern() { return this.mDestinationFileNamePattern; }
    set destinationFileNamePattern(pValue) { this.mDestinationFileNamePattern = String.verify(pValue); }

    constructor() {
        super((new ArgTemplateFactory()).create());
        this.sourceDirectoryPath = "";
        this.destinationDirectoryPath = "";
        this.sourceFileMask = "";
        this.destinationDirectoryNamePattern = "";
        this.destinationFileNamePattern = "";
    }

    assign() {
        super.assign();
        this.sourceDirectoryPath = this.get(ArgName.sourceDirectoryPath);
        this.destinationDirectoryPath = this.get(ArgName.destinationDirectoryPath);
        this.sourceFileMask = this.get(ArgName.sourceFileMask);
        this.destinationDirectoryNamePattern = this.get(ArgName.destinationDirectoryNamePattern);
        this.destinationFileNamePattern = this.get(ArgName.destinationFileNamePattern);
    }

    report(pMessenger, pIndentation) {
        pMessenger.sendPropertyInformation("Source Directory Path", this.sourceDirectoryPath, pIndentation);
        pMessenger.sendPropertyInformation("Destination Directory Path", this.destinationDirectoryPath, pIndentation);
        pMessenger.sendPropertyInformation("Source File Mask", this.sourceFileMask, pIndentation);
        pMessenger.sendPropertyInformation("Destination Directory Name Pattern", this.destinationDirectoryNamePattern, pIndentation);
        pMessenger.sendPropertyInformation("Destination File Name Pattern", this.destinationFileNamePattern, pIndentation);
        super.report(pMessenger, pIndentation);
    }    
    
}