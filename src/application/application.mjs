/**
 * @module "Application" class
 * @description Main application class
 */

import { Args } from "../application/args.mjs";
import { ConsoleApplication } from "fortah-console-library";
import { Logic } from "../logic/logic.mjs";

export class Application extends ConsoleApplication {

    constructor(pRootDirectoryPath) {
        super(pRootDirectoryPath, new Args());
    }    

    async runLogic() {
        const logic = new Logic();
        logic.run();
    }
}