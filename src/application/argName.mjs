/**
 * @module "ArgName" class
 * @description Enumerator with arg names
 */

import { Enum } from "fortah-core-library";
import { EnumItem } from "fortah-core-library";

export class ArgName {

    static get sourceDirectoryPath() { return "SourcePath"; }
    static get destinationDirectoryPath() { return "DestinationPath"; }
    static get sourceFileMask() { return "SourceFileMask"; }
    static get destinationDirectoryNamePattern() { return "DestinationDirectoryNamePattern"; }
    static get destinationFileNamePattern() { return "DestinationFileNamePattern"; }

    static get values() { return [
        new EnumItem(ArgName.sourceDirectoryPath),
        new EnumItem(ArgName.destinationDirectoryPath),
        new EnumItem(ArgName.sourceFileMask),
        new EnumItem(ArgName.destinationDirectoryNamePattern),
        new EnumItem(ArgName.destinationFileNamePattern)
    ]; }

    static parse(pText) {
        return Enum.parse(pText, ArgName.values, ArgName.name);
    }

    static toString(pValue) {
        return Enum.toString(pValue, ArgName.values, ArgName.name);
    }

}