/**
 * @module "ArgTemplateFactory" class
 * @description Creates arg templates
 */

import { ArgName } from "./argName.mjs";
import { ArgTemplateFactoryBase } from "fortah-core-library";
import { ArgTemplate } from "fortah-core-library";
import { DataType } from "fortah-core-library";

export class ArgTemplateFactory extends ArgTemplateFactoryBase {

    constructor() {
        super();
    }

    create() {
        const argTemplateArray = [
            new ArgTemplate(0, ArgName.sourceDirectoryPath, "Source directory path", DataType.string, true, true),
            new ArgTemplate(1, ArgName.destinationDirectoryPath, "Destination directory path", DataType.string, true, true),
            new ArgTemplate("m", ArgName.sourceFileMask, "Source file mask", DataType.string, false, false),
            new ArgTemplate("ddp", ArgName.destinationDirectoryNamePattern, "Destination directory name pattern", DataType.string, false, false),
            new ArgTemplate("dfp", ArgName.destinationFileNamePattern, "Destination file name pattern", DataType.string, false, false)
        ];
        let argTemplates = super.create();
        argTemplates.insert(argTemplateArray);
        return argTemplates;
    }
    
}