@echo off

set SourceFolderPath=.\tmp\src
set DestinationFolderPath=.\tmp\dst
set SourceFileMask=*.cal;*.txt
set DestinationDirectoryNamePattern={typePluralLowerCase}
set DestinationFileNamePattern={name} ({no}).cal

node "./src/main.mjs" "%SourceFolderPath%" "%DestinationFolderPath%" -m "%SourceFileMask%" -ddp "%DestinationDirectoryNamePattern%" -dfp "%DestinationFileNamePattern%"

pause